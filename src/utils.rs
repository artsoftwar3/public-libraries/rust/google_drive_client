use reqwest::blocking::Response;
use crate::models::server::error::GoogleError;

/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

pub fn extract_google_error(response: Response) -> GoogleError {
    let status_code = &response.status().as_u16().clone();
    let headers = &response.headers().clone();
    let response_content: reqwest::Result<String> = response.text();
    if response_content.is_err() {
        panic!("Unknown error, no message from google server");
    }
    let response_content: String = response_content.unwrap();
    if response_content.is_empty() {
        panic!("Unknown error, no content on response, status code {} headers {:?}", status_code, headers);
    }
    let google_error: serde_json::Result<GoogleError> = serde_json::from_str::<GoogleError>(response_content.as_str());
    if google_error.is_ok() {
        google_error.unwrap()
    } else {
        panic!("Unknown error: {}  headers {:?}", response_content, headers);
    }
}