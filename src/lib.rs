#![feature(proc_macro_hygiene, decl_macro)]
#![feature(in_band_lifetimes)]
#![feature(toowned_clone_into)]
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate log;
extern crate env_logger;
extern crate url;
/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

mod errors;
mod handlers;
mod models;
mod provider;
mod utils;

pub use provider::{GoogleDriveClient, GoogleDriveClientParams};
pub use models::client::error::GoogleClientError;
pub use models::client::file_upload::DriveFileUpload;
pub use models::client::file_upload::DriveFileMetadata;
pub use models::client::file_download::FileDownloadResponse;
pub use models::client::file_upload::DriveFolderUpload;
pub use models::client::file_search::DriveFileSearch;
pub use models::client::file_search::DriveFileSearchBuilder;
pub use models::client::chunk_size::{ChunkSize, ChunkSizeUnitType};

pub use models::server::file::DriveFile;
pub use models::server::jwt::JWTAuthResponse;
pub use models::server::file_upload_response::FileUploadResponse;
pub use models::server::file_list::DriveFileList;
