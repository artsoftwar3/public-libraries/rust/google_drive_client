/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Deserialize, Serialize};
use crate::handlers::{upload, download, delete, search, find};
use crate::models::client::file_upload::{DriveFileUpload, DriveFolderUpload};
use crate::models::server::file_upload_response::FileUploadResponse;
use crate::models::client::error::{GoogleClientError};
use crate::models::client::file_search::DriveFileSearch;
use crate::models::server::file_list::DriveFileList;
use crate::models::client::file_download::FileDownloadResponse;
use crate::models::server::file::DriveFile;
use std::time::Duration;
use reqwest::blocking::{Client, ClientBuilder};
use crate::models::client::chunk_size::ChunkSize;
use reqwest::Proxy;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GoogleDriveClient {
    params: GoogleDriveClientParams
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GoogleDriveClientParams {
    pub request_timeout: Duration,
    pub chunk_size: Option<ChunkSize>,
    pub proxy: Option<String>
}

impl GoogleDriveClient {
    pub fn upload_file(self, drive_file: DriveFileUpload) -> Result<FileUploadResponse, GoogleClientError> {
        let chunk_size = self.params.chunk_size.clone();
        let client = self.setup_client();
        upload::handle_file_upload(client, &drive_file, chunk_size)
    }
    pub fn download_file(self, file_id: &String) -> Result<FileDownloadResponse, GoogleClientError> {
        download::handle_file_download(self.setup_client(), file_id)
    }
    pub fn create_folder(self, drive_folder: DriveFolderUpload) -> Result<String, GoogleClientError> {
        upload::handle_create_folder(self.setup_client(), &drive_folder)
    }
    pub fn delete_file_or_folder(self, file_or_folder_id: &String) -> Result<(), GoogleClientError> {
        delete::delete_file_or_folder(self.setup_client(), file_or_folder_id)
    }
    pub fn search(self, search: DriveFileSearch) -> Result<DriveFileList, GoogleClientError> {
        search::handle_search(self.setup_client(), search)
    }
    pub fn find(self, file_id: String) -> Result<Option<DriveFile>, GoogleClientError> {
        find::handle_find(self.setup_client(), file_id)
    }
    fn setup_client(self) -> Client {
        if self.params.proxy.is_some() {
            let proxy: String = self.params.proxy.unwrap();
            ClientBuilder::new()
                .proxy(Proxy::all(proxy.as_str()).unwrap())
                .timeout(self.params.request_timeout)
                .build().unwrap()
        } else {
            ClientBuilder::new().timeout(self.params.request_timeout).build().unwrap()
        }
    }
    pub fn new(params: GoogleDriveClientParams) -> GoogleDriveClient {
        GoogleDriveClient { params }
    }
}

impl Default for GoogleDriveClient {
    fn default() -> Self {
        GoogleDriveClient {
            params: GoogleDriveClientParams {
                request_timeout: Duration::from_secs(60),
                chunk_size: Some(ChunkSize::default()),
                proxy: None
            }
        }
    }
}