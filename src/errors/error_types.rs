/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use crate::models::client::error::{GoogleClientError};
use crate::models::server::error::GoogleError;

pub fn reading_service_account_json() -> GoogleClientError {
    GoogleClientError::build()
        .with_description("Error reading service account file file.")
        .done()
}

pub fn service_account_file_not_found(service_account_env_var: &String, service_account_file_name: &String) -> GoogleClientError {
    GoogleClientError::build()
        .with_description(format!("You need to specify {} env variable with the secret content or put a secret json file in your HOME called {}",
                             service_account_env_var, service_account_file_name).as_str())
        .done()
}

pub fn fail_requesting_token(error_message: String) -> GoogleClientError {
    GoogleClientError::build()
        .with_description(format!("{} -> {}", "Error Requesting Google OAuth V2 Token", error_message).as_str())
        .done()
}

pub fn fail_downloading_file(error_message: String) -> GoogleClientError {
    GoogleClientError::build()
        .with_description(error_message.as_str())
        .done()
}

pub fn fail_creating_folder(google_error: GoogleError) -> GoogleClientError {
    GoogleClientError::build()
        .with_description("Error trying to create a folder in server")
        .with_cause(google_error)
        .done()
}

pub fn fail_trying_to_create_folder(error_message: &String) -> GoogleClientError {
    GoogleClientError::build()
        .with_description(error_message.as_str())
        .done()
}

pub fn fail_deleting_file(error_message: String) -> GoogleClientError {
    GoogleClientError::build()
        .with_description(error_message.as_str())
        .done()
}

pub fn fail_sharing_file(account: &String) -> GoogleClientError {
    GoogleClientError::build()
        .with_description(format!("Fail to share the file with account {}", account).as_str())
        .done()
}

pub fn fail_sharing_folder(account: &String) -> GoogleClientError {
    GoogleClientError::build()
        .with_description(format!("Fail to share the folder with account {}", account).as_str())
        .done()
}

pub fn fail_uploading_file(google_error: GoogleError) -> GoogleClientError {
    GoogleClientError::build()
        .with_description("Fail to upload file")
        .with_cause(google_error)
        .done()
}

pub fn fail_getting_files(google_error: GoogleError) -> GoogleClientError {
    GoogleClientError::build()
        .with_description("Fail to get the file list")
        .with_cause(google_error)
        .done()
}

pub fn custom_error(custom_message: &String) -> GoogleClientError {
    GoogleClientError::build()
        .with_description(custom_message.as_str())
        .done()
}