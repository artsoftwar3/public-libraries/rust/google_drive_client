use rocket::{Request, Response};
use rocket::http::Status;
use rocket::response::{Responder};
use std::io::Cursor;
use crate::{FileDownloadResponse, FileUploadResponse, DriveFileList, GoogleClientError};
use serde::{Serialize, Deserialize};
use crate::models::server::file::DriveFile;

pub struct CommonResponders {}
impl CommonResponders {
    pub fn json_responder<T>(type_instance: T, _: &Request) -> Result<Response<'static>, Status> where T: Serialize + Deserialize<'static> {
        let body_content = serde_json::to_string(&type_instance).unwrap();
        Response::build()
            .raw_header("Content-Type", "application/json")
            .sized_body(Cursor::new(body_content))
            .ok()
    }
}

impl Responder<'static> for FileDownloadResponse {
    fn respond_to(self, _: &Request) -> Result<Response<'static>, Status> {
        Response::build()
            .raw_header("Content-Type", self.file_data.mime_type.unwrap())
            .raw_header("Content-Disposition", format!("attachment; filename={}", self.file_data.name.unwrap()))
            .sized_body(Cursor::new(self.content))
            .ok()
    }
}

impl Responder<'static> for FileUploadResponse {
    fn respond_to(self, req: &Request) -> Result<Response<'static>, Status> {
        CommonResponders::json_responder(self.clone(), req)
    }
}

impl Responder<'static> for DriveFile {
    fn respond_to(self, req: &Request) -> Result<Response<'static>, Status> {
        CommonResponders::json_responder(self, req)
    }
}

impl Responder<'static> for DriveFileList {
    fn respond_to(self, req: &Request) -> Result<Response<'static>, Status> {
        CommonResponders::json_responder(self, req)
    }
}

impl Responder<'static> for GoogleClientError {
    fn respond_to(self, req: &Request) -> Result<Response<'static>, Status> {
        CommonResponders::json_responder(self, req)
    }
}