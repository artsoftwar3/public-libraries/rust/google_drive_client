/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Deserialize, Serialize};
use serde_json::value::{Map, Value};
use crate::models::server::capabilities::DriveCapabilities;
use crate::models::server::content_hints::DriveContentHints;
use crate::models::server::image_media_metadata::DriveImageMediaMetadata;
use crate::models::server::permission::DrivePermission;
use crate::models::server::user::DriveUser;
use crate::models::server::video_media_metadata::DriveVideoMediaMetadata;

// Structure extracted from documentation here: https://developers.google.com/drive/api/v3/reference/files#resource
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DriveFile {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "mimeType", skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starred: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trashed: Option<bool>,
    #[serde(rename = "explicitlyTrashed", skip_serializing_if = "Option::is_none")]
    pub explicitly_trashed: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub parents: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<Map<String, Value>>,
    #[serde(rename = "appProperties", skip_serializing_if = "Option::is_none")]
    pub app_properties: Option<Map<String, Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub spaces: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub version: Option<isize>,
    #[serde(rename = "webContentLink", skip_serializing_if = "Option::is_none")]
    pub web_content_link: Option<String>,
    #[serde(rename = "webViewLink", skip_serializing_if = "Option::is_none")]
    pub web_view_link: Option<String>,
    #[serde(rename = "iconLink", skip_serializing_if = "Option::is_none")]
    pub icon_link: Option<String>,
    #[serde(rename = "thumbnailLink", skip_serializing_if = "Option::is_none")]
    pub thumbnail_link: Option<String>,
    #[serde(rename = "viewedByMe", skip_serializing_if = "Option::is_none")]
    pub viewed_by_me: Option<bool>,
    #[serde(rename = "viewedByMeTime", skip_serializing_if = "Option::is_none")]
    pub viewed_by_me_time: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "modifiedTime", skip_serializing_if = "Option::is_none")]
    pub modified_time: Option<String>,
    #[serde(rename = "modifiedByMeTime", skip_serializing_if = "Option::is_none")]
    pub modified_by_me_time: Option<String>,
    #[serde(rename = "sharedWithMeTime", skip_serializing_if = "Option::is_none")]
    pub shared_with_me_time: Option<String>,
    #[serde(rename = "sharingUser", skip_serializing_if = "Option::is_none")]
    pub sharing_user: Option<DriveUser>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owners: Option<Vec<DriveUser>>,
    #[serde(rename = "lastModifyingUser", skip_serializing_if = "Option::is_none")]
    pub last_modifying_user: Option<DriveUser>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shared: Option<bool>,
    #[serde(rename = "ownedByMe", skip_serializing_if = "Option::is_none")]
    pub owned_by_me: Option<bool>,
    #[deprecated(
        since = "v3",
        note = "use copyRequiresWriterPermission instead."
    )]
    #[serde(rename= "viewersCanCopyContent", skip_serializing_if = "Option::is_none")]
    pub viewers_can_copy_content: Option<bool>,
    #[serde(rename = "writersCanShare", skip_serializing_if = "Option::is_none")]
    pub writers_can_share: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub permissions: Option<Vec<DrivePermission>>,
    #[serde(rename = "folderColorRgb", skip_serializing_if = "Option::is_none")]
    pub folder_color_rgb: Option<String>,
    #[serde(rename = "originalFilename", skip_serializing_if = "Option::is_none")]
    pub original_filename: Option<String>,
    #[serde(rename = "fullFileExtension", skip_serializing_if = "Option::is_none")]
    pub full_file_extension: Option<String>,
    #[serde(rename = "fileExtension", skip_serializing_if = "Option::is_none")]
    pub file_extension: Option<String>,
    #[serde(rename = "md5Checksum", skip_serializing_if = "Option::is_none")]
    pub md5_checksum: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub size: Option<isize>,
    #[serde(rename = "quotaBytesUsed", skip_serializing_if = "Option::is_none")]
    pub quota_bytes_used: Option<isize>,
    #[serde(rename = "headRevisionId", skip_serializing_if = "Option::is_none")]
    pub head_revision_id: Option<String>,
    #[serde(rename = "contentHints", skip_serializing_if = "Option::is_none")]
    pub content_hints: Option<DriveContentHints>,
    #[serde(rename = "imageMediaMetadata", skip_serializing_if = "Option::is_none")]
    pub image_media_metadata: Option<DriveImageMediaMetadata>,
    #[serde(rename = "videoMediaMetadata", skip_serializing_if = "Option::is_none")]
    pub video_media_metadata: Option<DriveVideoMediaMetadata>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub capabilities: Option<DriveCapabilities>,
    #[serde(rename = "isAppAuthorized", skip_serializing_if = "Option::is_none")]
    pub is_app_authorized: Option<bool>,
    #[serde(rename = "hasThumbnail", skip_serializing_if = "Option::is_none")]
    pub has_thumbnail: Option<bool>,
    #[serde(rename = "thumbnailVersion", skip_serializing_if = "Option::is_none")]
    pub thumbnail_version: Option<isize>,
    #[serde(rename = "modifiedByMe", skip_serializing_if = "Option::is_none")]
    pub modified_by_me: Option<bool>,
    #[serde(rename = "trashingUser", skip_serializing_if = "Option::is_none")]
    pub trashing_user: Option<DriveUser>,
    #[serde(rename = "trashedTime", skip_serializing_if = "Option::is_none")]
    pub trashed_time: Option<String>,
    #[deprecated(
        since = "v3",
        note = "use driveId instead."
    )]
    #[serde(rename = "teamDriveId", skip_serializing_if = "Option::is_none")]
    pub team_drive_id: Option<String>,
    #[serde(rename = "hasAugmentedPermissions", skip_serializing_if = "Option::is_none")]
    pub has_augmented_permissions: Option<bool>,
    #[serde(rename = "permissionIds", skip_serializing_if = "Option::is_none")]
    pub permission_ids: Option<Vec<String>>,
    #[serde(rename = "exportLinks", skip_serializing_if = "Option::is_none")]
    pub export_links: Option<Map<String, Value>>,
    #[serde(rename = "driveId", skip_serializing_if = "Option::is_none")]
    pub drive_id: Option<String>
}