/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DriveImageMediaMetadata {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub width: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub height: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rotation: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location: Option<DriveImageMediaMetadataLocation>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DriveImageMediaMetadataLocation {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub latitude: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub longitude: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub altitude: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub time: Option<String>,
    #[serde(rename = "cameraMake", skip_serializing_if = "Option::is_none")]
    pub camera_make: Option<String>,
    #[serde(rename = "cameraModel", skip_serializing_if = "Option::is_none")]
    pub camera_model: Option<String>,
    #[serde(rename = "exposureTime", skip_serializing_if = "Option::is_none")]
    pub exposure_time: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aperture: Option<f64>,
    #[serde(rename = "flashUsed", skip_serializing_if = "Option::is_none")]
    pub flash_used: Option<bool>,
    #[serde(rename = "focalLength", skip_serializing_if = "Option::is_none")]
    pub focal_length: Option<f64>,
    #[serde(rename = "isoSpeed", skip_serializing_if = "Option::is_none")]
    pub iso_speed: Option<i64>,
    #[serde(rename = "meteringMode", skip_serializing_if = "Option::is_none")]
    pub metering_mode: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sensor: Option<String>,
    #[serde(rename = "exposureMode", skip_serializing_if = "Option::is_none")]
    pub exposure_mode: Option<String>,
    #[serde(rename = "colorSpace", skip_serializing_if = "Option::is_none")]
    pub color_space: Option<String>,
    #[serde(rename = "whiteBalance", skip_serializing_if = "Option::is_none")]
    pub white_balance: Option<String>,
    #[serde(rename = "exposureBias", skip_serializing_if = "Option::is_none")]
    pub exposure_bias: Option<f64>,
    #[serde(rename = "maxApertureValue", skip_serializing_if = "Option::is_none")]
    pub max_aperture_value: Option<f64>,
    #[serde(rename = "subjectDistance", skip_serializing_if = "Option::is_none")]
    pub subject_distance: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lens: Option<String>
}