/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ChunkSize {
    bytes: usize
}

pub enum ChunkSizeUnitType {
    BYTES, KB, MB, GB, TB
}

impl ChunkSize {
    pub fn new(unit: usize, unit_type: ChunkSizeUnitType) -> ChunkSize {
        ChunkSize {
            bytes: get_byte_count(unit, unit_type)
        }
    }
}

impl Default for ChunkSize {
    fn default() -> Self {
        ChunkSize {
            bytes: get_byte_count(10, ChunkSizeUnitType::MB)
        }
    }
}

impl Into<usize> for ChunkSize {
    fn into(self) -> usize {
        self.bytes
    }
}

impl From<usize> for ChunkSize {
    fn from(bytes: usize) -> Self {
        ChunkSize {
            bytes
        }
    }
}

fn get_byte_count(unit: usize, chunk_size_unit_type: ChunkSizeUnitType) -> usize {
    return match chunk_size_unit_type {
        ChunkSizeUnitType::BYTES => unit,
        ChunkSizeUnitType::KB => unit * 1024,
        ChunkSizeUnitType::MB => get_byte_count(unit, ChunkSizeUnitType::KB) * 1024,
        ChunkSizeUnitType::GB => get_byte_count(unit, ChunkSizeUnitType::MB) * 1024,
        ChunkSizeUnitType::TB => get_byte_count(unit, ChunkSizeUnitType::GB) * 1024,
    }
}