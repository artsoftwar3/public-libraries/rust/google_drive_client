/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use std::io::Read;
use reqwest::blocking::{Client};
use crate::errors::error_types::fail_downloading_file;
use crate::handlers::token;
use crate::models::client::error::GoogleClientError;
use crate::models::client::file_download::FileDownloadResponse;
use crate::models::server::file::DriveFile;

pub fn handle_file_download(client: Client, file_id: &String) -> Result<FileDownloadResponse, GoogleClientError> {
    let file_content = get_content(&client, file_id)?;
    let file_data = get_file_data(&client, file_id)?;
    Ok(FileDownloadResponse {
        content: file_content,
        file_data
    })
}

fn get_file_data(client: &Client, file_id: &String) -> Result<DriveFile, GoogleClientError> {
    let url: String = format!("https://www.googleapis.com/drive/v3/files/{}", file_id);
    let token: String = token::get_token().unwrap();
    let result = client.get(url.as_str())
        .header("Authorization", format!("Bearer {}", token).as_str())
        .send();
    if result.is_err() {
        return Err(fail_downloading_file(result.err().unwrap().to_string()));
    }
    let drive_file = result.unwrap().json::<DriveFile>().unwrap();
    Ok(drive_file)
}

fn get_content(client: &Client, file_id: &String) -> Result<Vec<u8>, GoogleClientError> {
    let url: String = format!("https://www.googleapis.com/drive/v3/files/{}?alt=media", file_id);
    let token: String = token::get_token().unwrap();
    let response = client.get(url.as_str())
        .header("Authorization", format!("Bearer {}", token).as_str())
        .send();
    if response.is_err() {
        return Err(fail_downloading_file(response.err().unwrap().to_string()));
    }
    let mut response_bytes: Vec<u8> = Vec::new();
    let result = response.unwrap().read_to_end(&mut response_bytes);
    if result.is_err() {
        return Err(fail_downloading_file(result.err().unwrap().to_string()))
    }
    Ok(response_bytes)
}