/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use reqwest::blocking::{Client};
use crate::errors::error_types::{custom_error, fail_creating_folder, fail_sharing_file, fail_sharing_folder, fail_trying_to_create_folder, fail_uploading_file};
use crate::handlers::token;
use crate::models::client::error::GoogleClientError;
use crate::models::client::file_upload::{DriveFileUpload, DriveFolderUpload};
use crate::models::server::file_upload_response::FileUploadResponse;
use crate::models::server::permission::DrivePermission;
use crate::models::server::resumable_session::{DriveResumableSessionRequest, DriveResumableSessionResponse};
use std::str::FromStr;
use crate::models::client::chunk_size::ChunkSize;
use crate::utils::extract_google_error;
use std::slice::Chunks;

const RESUMABLE_FILE_UPLOAD_SESSION_REQUEST_URL: &str = "https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable";
const GDRIVE_FOLDER_MIME_TYPE: &str = "application/vnd.google-apps.folder";
const GDRIVE_SHARE_ACCOUNT: &str = "GDRIVE_SHARE_ACCOUNT";

pub fn handle_create_folder(client: Client, drive_folder: &DriveFolderUpload) -> Result<String, GoogleClientError> {
    let token: String = token::get_token().unwrap();
    let location = get_resumable_session_location_for_folder(&client, &token, drive_folder);
    let result = client.post(location.as_str())
        .header("Content-Type", GDRIVE_FOLDER_MIME_TYPE)
        .header("Content-Length", 0)
        .send();
    if result.is_ok() {
        let response = result.unwrap();
        if response.status().is_success() {
            let share_account = dotenv::var(GDRIVE_SHARE_ACCOUNT);
            let folder_created = response.json::<DriveResumableSessionResponse>().unwrap();
            let folder_id = folder_created.id;
            if share_account.is_ok() {
                let account = share_account.unwrap();
                return if share_file(&client, account.as_str(), &folder_id, &token) {
                    Ok(String::from(&folder_id))
                } else {
                    Err(fail_sharing_folder(&account))
                }
            }
            return Ok(String::from(&folder_id));
        }
        return Err(fail_creating_folder(extract_google_error(response)));
    }
    Err(fail_trying_to_create_folder(&result.err().unwrap().to_string()))
}

pub fn handle_file_upload(client: Client, drive_file: &DriveFileUpload, chunk_size: Option<ChunkSize>) -> Result<FileUploadResponse, GoogleClientError> {
    let token = token::get_token().unwrap();
    let session_location = get_resumable_session_location_for_file(&client, &token, drive_file);
    if chunk_size.is_some() {
        // multiple chunks upload
        handle_multiple_chunk_upload(&client, drive_file, session_location, chunk_size.unwrap(), &token)
    } else {
        handle_single_upload(&client, drive_file, session_location, &token)
    }
}

fn handle_multiple_chunk_upload(client: &Client, drive_file: &DriveFileUpload, session_location: String, chunk_size: ChunkSize, token: &String) -> Result<FileUploadResponse, GoogleClientError> {

    let chunk_size = chunk_size.into();

    if chunk_size <= 0 {
        return handle_single_upload(&client, drive_file, session_location, token);
    }

    let file_size: usize = drive_file.bytes.len();

    let mut chunk_start: usize = get_chunk_start_from_session(&client, session_location.clone());

    let chunks: Chunks<'_, u8>;

    if chunk_start > 0 {
        chunks = drive_file.bytes[chunk_start..].chunks(chunk_size);
    } else {
        chunks = drive_file.bytes.chunks(chunk_size);
    }

    if chunks.len() <= 0 {
        panic!("No chunks available to upload on file.");
    }

    let mut session_result: Option<DriveResumableSessionResponse> = None;

    for chunk in chunks {
        session_result = handle_upload_chunk(&client, &file_size, &chunk_start, chunk, session_location.clone(), drive_file.metadata.mime_type.as_str())?;
        if session_result.is_some() {
            break;
        }
        chunk_start = chunk_start + chunk_size;
    }

    debug!("Multiple chunks uploaded");

    return if session_result.is_some() {
        let session = session_result.unwrap();
        let share_account = dotenv::var(GDRIVE_SHARE_ACCOUNT);
        if share_account.is_ok() {
            let account = share_account.unwrap();
            debug!("File id to share {}", &session.id);
            if share_file(&client, account.as_str(), &session.id, &token) {
                Ok(FileUploadResponse::from(session))
            } else {
                Err(fail_sharing_file(&account))
            }
        } else {
            Ok(FileUploadResponse::from(session))
        }
    } else {
        Err(custom_error(&"Error unknown: Uncompleted upload".to_string()))
    }
}

fn get_chunk_start_from_session(client: &Client, session_location: String) -> usize {
    let file_status_response = client.put(session_location.as_str())
        .header("Content-Length", 0)
        .send().unwrap();
    let status_code = &file_status_response.status();
    let response_headers = file_status_response.headers();
    if status_code.is_success() {
        return 0;
    }
    let status_code = status_code.as_u16();
    if status_code == 308 {
        // incomplete file
        if response_headers.contains_key("Range") {
            let range = response_headers.get("Range").unwrap();
            let range = range.to_str().unwrap().to_string();
            let start_at = range.chars().skip(8).collect::<String>();
            let chunk_start = usize::from_str(start_at.as_str()).unwrap();
            return chunk_start;
        }
    } else if status_code >= 400 {
        let google_error = extract_google_error(file_status_response);
        panic!("Error trying to get status {:?}", google_error);
    }
    return 0
}

fn get_content_range_header(file_size: &usize, chunk_start: &usize, chunk_end: &usize) -> String {
    format!("bytes {}-{}/{}", chunk_start, chunk_end, file_size)
}

fn handle_upload_chunk(client: &Client, file_size: &usize, chunk_start: &usize, chunk: &[u8], session_location: String, mime_type: &str) -> Result<Option<DriveResumableSessionResponse>, GoogleClientError> {
    let mut buffer: Vec<u8> = Vec::new();
    buffer.extend_from_slice(chunk);
    let chunk_size: usize = chunk.len();
    let chunk_end: usize = chunk_start + (chunk_size - 1);
    let chunk_upload_response = client.put(session_location.as_str())
        .header("Content-Length", chunk_size.to_string())
        .header("Content-Type", mime_type.to_string())
        .header("Content-Range", get_content_range_header(&file_size, &chunk_start, &chunk_end))
        .body(buffer)
        .send();
    return if chunk_upload_response.is_ok() {
        let response = chunk_upload_response.unwrap();
        debug!("Chunk response {:?}", response);
        if response.status().is_success() {
            let session: DriveResumableSessionResponse = response.json::<DriveResumableSessionResponse>().unwrap();
            Ok(Some(session))
        } else {
            let status_code = &response.status().as_u16();
            if status_code.eq(&308) {
                let range = &response.headers().get("Range").unwrap().to_str().unwrap();
                debug!("RANGE PENDING {}", range.clone());
                Ok(None)
            } else {
                Err(fail_uploading_file(extract_google_error(response)))
            }
        }
    } else {
        Err(custom_error(&chunk_upload_response.err().unwrap().to_string()))
    }
}

fn handle_single_upload(client: &Client, drive_file: &DriveFileUpload, session_location: String, token: &String) -> Result<FileUploadResponse, GoogleClientError> {
    let mut bytes: Vec<u8> = Vec::new();
    bytes.extend_from_slice(drive_file.bytes.as_slice());
    let file_upload_response = client.put(session_location.as_str())
        .header("Content-Length", drive_file.bytes.len().to_string())
        .header("Content-Type", drive_file.metadata.mime_type.as_str())
        .body(bytes)
        .send();
    debug!("File uploaded {:?}", file_upload_response);
    return if file_upload_response.is_ok() {
        let response = file_upload_response.unwrap();
        let share_account = dotenv::var(GDRIVE_SHARE_ACCOUNT);
        debug!("File uploaded {:?}", response);
        if response.status().is_success() {
            let session: DriveResumableSessionResponse = response.json::<DriveResumableSessionResponse>().unwrap();
            if share_account.is_ok() {
                let account = share_account.unwrap();
                debug!("File id to share {}", &session.id);
                if share_file(&client, account.as_str(), &session.id, &token) {
                    Ok(FileUploadResponse::from(session))
                } else {
                    Err(fail_sharing_file(&account))
                }
            } else {
                Ok(FileUploadResponse::from(session))
            }
        } else {
            Err(fail_uploading_file(extract_google_error(response)))
        }
    } else {
        Err(custom_error(&file_upload_response.err().unwrap().to_string()))
    }
}

fn get_resumable_session_location_for_file(client: &Client, token: &String, drive_file: &DriveFileUpload) -> String {
    let session_request: DriveResumableSessionRequest;
    if drive_file.metadata.parent_id.is_some() {
        let parent_id = drive_file.metadata.parent_id.clone().unwrap();
        session_request = DriveResumableSessionRequest {
            name: String::from(&drive_file.metadata.file_name),
            mime_type: String::from(&drive_file.metadata.mime_type),
            description: String::from(&drive_file.metadata.description),
            parents: Some(vec![parent_id])
        };
    } else {
        session_request = DriveResumableSessionRequest {
            name: String::from(&drive_file.metadata.file_name),
            mime_type: String::from(&drive_file.metadata.mime_type),
            description: String::from(&drive_file.metadata.description),
            parents: None
        };
    }
    let resumable_session_response = client.post(RESUMABLE_FILE_UPLOAD_SESSION_REQUEST_URL)
        .header("X-Upload-Content-Type", String::from(&drive_file.metadata.mime_type).as_str())
        .header("X-Upload-Content-Length", String::from(&drive_file.bytes.len().to_string()).as_str())
        .header("Content-Type", "application/json; charset=UTF-8")
        .header("Content-Length", 0)
        .header("Authorization", format!("Bearer {}", token).as_str())
        .body(json!(session_request).to_string())
        .send().unwrap();
    return resumable_session_response.headers().get("Location").unwrap().to_str().unwrap().to_string();
}

fn get_resumable_session_location_for_folder(client: &Client, token: &String, drive_folder: &DriveFolderUpload) -> String {
    let mut folder_parents: Option<Vec<String>> = None;
    if drive_folder.parent_id.is_some() {
        let folder_parent_id = drive_folder.parent_id.clone().unwrap();
        folder_parents = Some(vec![folder_parent_id]);
    }
    let session_request: DriveResumableSessionRequest = DriveResumableSessionRequest {
        name: String::from(&drive_folder.name),
        mime_type: String::from(GDRIVE_FOLDER_MIME_TYPE),
        description: String::from(&drive_folder.description),
        parents: folder_parents
    };
    let resumable_session_response = client.post(RESUMABLE_FILE_UPLOAD_SESSION_REQUEST_URL)
        .header("X-Upload-Content-Type", String::from(GDRIVE_FOLDER_MIME_TYPE).as_str())
        .header("X-Upload-Content-Length", 0)
        .header("Content-Type", "application/json; charset=UTF-8")
        .header("Content-Length", 0)
        .header("Authorization", String::from("Bearer ") + token.as_str())
        .body(json!(session_request).to_string())
        .send().unwrap();
    return resumable_session_response.headers().get("Location").unwrap().to_str().unwrap().to_string();
}

fn share_file(client: &Client, account_email: &str, file_id: &String, token: &String) -> bool {
    let url: String = format!("https://www.googleapis.com/drive/v3/files/{}/permissions", file_id);
    let permission_request = DrivePermission::writer(account_email);
    let permission_response = client.post(url.as_str())
        .header("Content-Type", "application/json; charset=UTF-8")
        .header("Authorization", String::from("Bearer ") + token.as_str())
        .body(json!(permission_request).to_string())
        .send();
    return if permission_response.is_ok() {
        let response = permission_response.unwrap();
        if response.status().is_success() {
            true
        } else {
            let error = extract_google_error(response);
            error!("Error sharing file {:?}", error);
            false
        }
    } else {
        error!("Error sharing file {:?}", permission_response.err().unwrap());
        false
    }
}