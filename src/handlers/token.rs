/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use reqwest::blocking::{Client, ClientBuilder};
use std::collections::HashMap;
use std::path::Path;
use std::fs;
use crate::models::client::error::GoogleClientError;
use crate::errors::error_types::{fail_requesting_token, custom_error};
use crate::models::server::jwt::{JWT, JWTAuthResponse};
use crate::models::server::token_info_response::GoogleTokenInfoResponse;
use crate::models::server::oauth_response::OAuth2Response;


const TOKEN_FILE_PATH: &str = "/tmp/gdrive/token.json";
const TEMP_FOLDER: &str = "/tmp/gdrive";
const CHECK_TOKEN_URI: &str = "https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=";

pub fn get_token() -> Result<String, GoogleClientError> {
    let stored_token = get_stored_token();
    if stored_token.is_some() {
        let token = stored_token.unwrap();
        if check_token(&token.access_token) {
            return Ok(token.access_token);
        }
    }
    let fresh_token= refresh_token();
    if fresh_token.is_err() {
        return Err(fresh_token.err().unwrap());
    }

    Ok(fresh_token.unwrap().access_token)
}
fn check_token(token: &String) -> bool {
    let client: Client = ClientBuilder::new().build().unwrap();
    let uri = String::from(CHECK_TOKEN_URI) + token;
    let response = client.get(uri.as_str())
        .header("Host", "www.googleapis.com").send();
    if response.is_err() {
        warn!("Error checking token");
        return false;
    }
    let token_validation = response.unwrap().json::<GoogleTokenInfoResponse>();
    if token_validation.is_ok() {
        let valid_token = token_validation.unwrap();
        if valid_token.expires_in <= 0 {
            info!("Token expired! Refreshing token");
            return false;
        } else {
            return true;
        }
    }
    false
}
fn refresh_token() -> Result<JWTAuthResponse, GoogleClientError> {
    let jwt: JWT = JWT::new()?;
    let client: Client = ClientBuilder::new().build().unwrap();
    let mut params = HashMap::new();
    params.insert("grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer");
    params.insert("assertion", jwt.jwt_encoded.as_str());
    let response = client.post(jwt.claim_set.aud.as_str())
        .form(&params)
        .header("Host", "www.googleapis.com")
        .send();
    if response.is_err() {
        return Err(fail_requesting_token(response.err().unwrap().to_string()));
    }
    let result = response.unwrap().json::<OAuth2Response>().unwrap().get_result();

    store_token(result)
}
fn get_stored_token() -> Option<JWTAuthResponse> {
    let token_path = Path::new(TOKEN_FILE_PATH);
    if token_path.exists() {
        info!("Stored token found");
        let token_file_string = fs::read_to_string(token_path);
        if token_file_string.is_err() {
            warn!("Error reading token from file {}", &token_file_string.err().unwrap());
            return None;
        } else {
            info!("Reading stored token");
            let token = serde_json::from_str::<JWTAuthResponse>(&token_file_string.unwrap().as_str()).unwrap();
            return Some(token);
        }
    } else {
        info!("No stored token found.");
    }

    None
}
fn store_token(token_response: Result<JWTAuthResponse, GoogleClientError>) -> Result<JWTAuthResponse, GoogleClientError> {
    if token_response.is_ok() {
        let token: JWTAuthResponse = token_response.unwrap();
        let temp_folder_path = Path::new(TEMP_FOLDER);
        if !temp_folder_path.exists() {
            let folder_created = fs::create_dir_all(temp_folder_path);
            if folder_created.is_err() {
                warn!("Error creating folder for token {}", &folder_created.err().unwrap());
            }
        }
        let json_token_string = serde_json::to_string(&token).unwrap();
        debug!("JSON {}", &json_token_string);
        let stored = fs::write(TOKEN_FILE_PATH, &json_token_string);
        if stored.is_err() {
            warn!("Error trying to save token into file {}", &stored.err().unwrap());
            return Err(custom_error(&"Error trying to save token into file.".to_string()));
        }
        return Ok(token.to_owned());
    } else {
        let error: GoogleClientError = token_response.err().unwrap();
        return Err(error);
    }
}
