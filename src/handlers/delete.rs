/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use crate::handlers::token;
use reqwest::blocking::{Client};
use crate::errors::error_types::fail_deleting_file;
use crate::models::client::error::GoogleClientError;
use crate::models::server::error::GoogleError;

pub fn delete_file_or_folder(client: Client, file_id: &String) -> Result<(), GoogleClientError> {
    let token = token::get_token().unwrap();
    let url = format!("https://www.googleapis.com/drive/v3/files/{}", file_id);
    let result = client.delete(url.as_str())
        .header("Authorization", format!("Bearer {}", token).as_str())
        .send();
    if result.is_err() {
        return Err(fail_deleting_file(result.err().unwrap().to_string()));
    }
    let response = result.unwrap();
    if !response.status().is_success() {
        let error: GoogleError = response.json::<GoogleError>().unwrap();
        let message = format!("Error sharing file {:?}", &error);
        return Err(fail_deleting_file(message));
    }
    Ok(())
}